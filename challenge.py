import logging

from longest_sequence import longest_sequence_sort

logger = logging.getLogger(__name__)

candidate_method = longest_sequence_sort

# sequence = [randint(0, 100000) for i in range(1000000)]
# print(longest_sequence(sequence))
# print(longest_sequence_sort(sequence))

# print(longest_sequence_sort([randint(0, 100000) for i in range(100000)]))
# print(timeit('print(max([longest_sequence([randint(0, 100000) for i in range(10000000)]) for i in range(1)]))', setup="from __main__ import longest_sequence; from random import randint", number=1))
# print(timeit('print(max([longest_sequence_sort([randint(0, 100000) for i in range(10000000)]) for i in range(1)]))', setup="from __main__ import longest_sequence_sort; from random import randint",
#              number=1))
