import logging

logger = logging.getLogger(__name__)

def longest_sequence_sort(sequence):
    sequence.sort()
    max_length = 0
    length = 1
    previous = None
    for i in sequence:
        if previous == i:
            continue
        if previous is not None and i == previous + 1:
            length += 1
        else:
            max_length = max(max_length, length)
            length = 1
        previous = i
    max_length = max(max_length, length)
    return max_length